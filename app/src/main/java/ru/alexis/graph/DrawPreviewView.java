package ru.alexis.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class DrawPreviewView extends SurfaceView implements SurfaceHolder.Callback{
    private DrawThread drawThread;

    Paint paint;
    Paint paintText;
    Path path;
    float length;
    int dx;
    int count;
    int max;
    int min;
    List<Integer> points = new ArrayList<>();
    private Rect previewRect = new Rect();
    private Paint mPaint = new Paint();
    private String TAG = DrawPreviewView.class.getSimpleName();

    public DrawPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    public DrawPreviewView(Context context){
        super(context);
        init();
    }

    public DrawPreviewView(Context context, WindowManager wm) {
        super(context);
        init();

    }

    void init(){
        getHolder().addCallback(this);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
        paint.setColor(Color.BLUE);

        paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintText.setTextSize(30);

        points.add(200);
        points.add(-100);
        points.add(100);
        points.add(-100);
        points.add(100);

        count = points.size();
        Point point = new Point();
        point.x = getContext().getResources().getDisplayMetrics().widthPixels;
        point.y = getContext().getResources().getDisplayMetrics().heightPixels;
        //point.set(1080, 720);
        mPaint = new Paint();
        mPaint.setColor(getResources().getColor(R.color.transparentBlack));
        int left = 0;
        int top = 200;//(getHeight() - 50);
        int right = getWidth();
        int bottom = getHeight();
        previewRect = new Rect(left, top, right, bottom);
        dx = 10;//point.x/(count);
        path = new Path();
        path.moveTo(20, 20);
        Log.d(TAG, "init: " + point.x);
        for(int i=0; i<1000; i+=5){
            path.rLineTo(5 + i, 5 +i);
        }
//        for (int pointY : points) {
//            path.rLineTo(dx, pointY);
//        }
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(getHolder());
        drawThread.setRunning(true);
        drawThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        drawThread.setRunning(false);
        while (retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    class DrawThread extends Thread {

        private boolean running = false;
        private SurfaceHolder surfaceHolder;

        public DrawThread(SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        public void run() {
            Canvas canvas;
            while (running) {
                canvas = null;
                try {
                    canvas = surfaceHolder.lockCanvas(null);
                    if (canvas == null)
                        continue;
//                    canvas.drawColor(Color.TRANSPARENT);
                    canvas.drawARGB(80, 102, 204, 255);

                    canvas.drawPath(path, mPaint);
                    canvas.save();
                    canvas.drawRect(previewRect, mPaint);
//            Bitmap descBitmap = Bitmap.createBitmap(right - left, bottom - top, Bitmap.Config.ARGB_8888);



                } finally {
                    if (canvas != null) {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
