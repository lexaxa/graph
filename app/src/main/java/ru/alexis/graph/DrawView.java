package ru.alexis.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class DrawView extends SurfaceView implements SurfaceHolder.Callback {

    private DrawThread drawThread;

    Paint paint;
    Paint paintText;
    Path path;
    PathMeasure pMeasure;
    float length;
    int dx;
    int count;
    int max;
    int min;
    List<Integer> points = new ArrayList<>();
    private Path path2;

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    public DrawView(Context context){
        super(context);
        init();
    }

    public DrawView(Context context, WindowManager wm) {
        super(context);
        init();

    }
    void init(){
        getHolder().addCallback(this);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setColor(Color.RED);
        paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintText.setTextSize(30);

        Point point = new Point();
        point.x = getContext().getResources().getDisplayMetrics().widthPixels;
        point.y = getContext().getResources().getDisplayMetrics().heightPixels;

        points.add(100);
        points.add(-100);
        points.add(100);
        points.add(-100);
        points.add(50);

        count = points.size();
        dx = point.x/(count);
        path = new Path();
        path2 = new Path();
        path.moveTo(0, 300);
        for (int pointY : points) {
            path.rLineTo(dx, pointY);
        }
        for(int i=0; i<point.x; i+=5){
            path2.rLineTo(5 + i, 5 +i);
        }
        pMeasure = new PathMeasure(path, false);
        length = pMeasure.getLength();
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(getHolder());
        drawThread.setRunning(true);
        drawThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        drawThread.setRunning(false);
        while (retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    class DrawThread extends Thread {

        private boolean running = false;
        private SurfaceHolder surfaceHolder;

        public DrawThread(SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        public void run() {
            Canvas canvas;
            while (running) {
                canvas = null;
                try {
                    canvas = surfaceHolder.lockCanvas(null);
                    if (canvas == null)
                        continue;
                    canvas.drawColor(Color.TRANSPARENT);
//                    canvas.drawARGB(80, 102, 204, 255);

                    canvas.drawPath(path, paint);
                    canvas.drawPath(path2, paint);

                    canvas.drawText(String.format("Length: %s", length), 100, 100,
                            paintText);
                } finally {
                    if (canvas != null) {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
