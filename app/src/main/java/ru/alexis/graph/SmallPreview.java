package ru.alexis.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class SmallPreview extends View implements View.OnTouchListener{

    private Path path;
    private Path path1;
    private Paint paint;
    private Paint paintFog;
    private Rect rectPreview;
    private Point touchPoint = new Point();
    private PorterDuffXfermode xfermode;
    private String mode;
    private boolean isDrag;
    private boolean isDragLeft;
    private boolean isDragRight;

    public SmallPreview(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SmallPreview(Context context) {
        super(context);
        init();
    }
    public void init(){
        setOnTouchListener(this);
        rectPreview = new Rect();
        rectPreview.set(0, 0, 200, 200);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(3);
        paint.setColor(Color.WHITE);

        paintFog = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintFog.setStyle(Paint.Style.STROKE);
        paintFog.setStrokeWidth(3);
        paintFog.setColor(Color.RED);

        path = new Path();
        path1 = new Path();
        path1.reset();
        path1.moveTo(20, 20);

        for(int i=0; i<50; i+=5){
            path1.rLineTo(5 + i, 5 +i);
        }
        xfermode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);

        // две пересекающиеся линии
        path1.moveTo(50,50);
        path1.lineTo(500,250);
        path1.moveTo(500,50);
        path1.lineTo(50,250);

    }
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        canvas.drawPaint(paintFog);

        canvas.drawRect(rectPreview, paint);
        paint.setXfermode(xfermode);
        canvas.drawPath(path, paintFog);
        canvas.drawPath(path1, paintFog);

//        System.out.println(canvas.getWidth());
//        System.out.println(canvas.getHeight());

        canvas.drawARGB(180, 255, 100, 155);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchPoint.set((int) event.getX(), (int) event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                touchPoint.set((int) event.getX(), (int) event.getY());

                break;
            case MotionEvent.ACTION_UP:
                isDragLeft = false;
                isDragRight = false;
                break;
        }
        if (!isDragLeft && touchPoint.x > rectPreview.left - 10 && touchPoint.x < rectPreview.left + 10) {
            isDragRight = false;
            isDragLeft = true;
        }else if (!isDragRight && touchPoint.x > rectPreview.right - 10 && touchPoint.x < rectPreview.right + 10) {
            isDragLeft = false;
            isDragRight = true;
        }else{
            isDragLeft = false;
            isDragRight = false;
            rectPreview.set(touchPoint.x - 100, 0, touchPoint.x + 100, 200);
        }
        if(isDragLeft){
            rectPreview.left = touchPoint.x;
        }
        if(isDragRight){
            rectPreview.right = touchPoint.x;
        }
        System.out.println(event.getAction() + "="+ isDragLeft + "=" + isDragRight);
        invalidate();
        return true;
    }


    public void setMode(String mode) {
        this.mode = mode;
        xfermode = new PorterDuffXfermode(PorterDuff.Mode.valueOf(mode));
        invalidate();
    }
}
