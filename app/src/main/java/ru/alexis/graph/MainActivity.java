package ru.alexis.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener {

    final String TAG = "myLogs";
    private DrawView surfaceView;
//    private DrawPreviewView surfaceView2;
    private SmallPreview smallPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        LinearLayout rootView = new LinearLayout(this);
//        rootView.setOrientation(LinearLayout.VERTICAL);
//        LinearLayout.LayoutParams rootParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        rootView.setLayoutParams(rootParams);
//
//        LinearLayout.LayoutParams chooseGraphsParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//        LinearLayout.LayoutParams canvasParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 700 );
//        LinearLayout.LayoutParams canvasPreviewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 700 );
//
//        CheckBox checkBox = new CheckBox(this);
//        checkBox.setText("Graph1");
//        checkBox.setLayoutParams(chooseGraphsParams);
//
//        CheckBox checkBox2 = new CheckBox(this);
//        checkBox2.setText("Graph2");
//        checkBox2.setLayoutParams(chooseGraphsParams);
//
//        rootView.addView(new DrawPreviewView(this, getWindowManager()), canvasPreviewParams);
//        rootView.addView(new DrawView(this, getWindowManager()), canvasParams);
//        rootView.addView(checkBox);
//        rootView.addView(checkBox2);

//        setContentView(rootView);
        setContentView(R.layout.activity_main);
        surfaceView = findViewById(R.id.surfaceView);
//        surfaceView2 = findViewById(R.id.surfaceView2);
        smallPreview = findViewById(R.id.smallPreview);

        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();
        // получаем размеры экрана
        Display display = getWindowManager().getDefaultDisplay();

        Point point = new Point();
        display.getSize(point);

        boolean widthIsMax = display.getWidth() > display.getHeight();
        // RectF экрана, соотвествует размерам экрана
        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());

        // RectF первью
        if (widthIsMax) {
            // превью в горизонтальной ориентации
            rectPreview.set(0, 0, point.x, point.y);
        } else {
            // превью в вертикальной ориентации
            rectPreview.set(0, 0, point.y, point.x);
        }
        smallPreview.setLayoutParams(new LinearLayout.LayoutParams((int)rectPreview.right,200 ));
        surfaceView.getLayoutParams().height = (int) (rectPreview.bottom);
        surfaceView.getLayoutParams().width = (int) (rectPreview.right);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.mods_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setOnItemSelectedListener(this);
        spinner.setAdapter(adapter);

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        System.out.println(parent.getItemAtPosition(pos));
        smallPreview.setMode((String)parent.getItemAtPosition(pos));
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

}
